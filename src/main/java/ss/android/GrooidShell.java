/*
    Copyright (C) 2012, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
 package ss.android;

import android.util.Log;

import com.android.dx.Version;
import com.android.dx.dex.DexFormat;
import com.android.dx.dex.DexOptions;
import com.android.dx.dex.cf.CfOptions;
import com.android.dx.dex.cf.CfTranslator;
import com.android.dx.dex.code.PositionList;
import com.android.dx.dex.file.ClassDefItem;
import com.android.dx.dex.file.DexFile;

import org.codehaus.groovy.control.BytecodeProcessor;
import org.codehaus.groovy.control.CompilerConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import dalvik.system.DexClassLoader;
import groovy.lang.GrooidClassLoader;

/**
 * A shell capable of executing Groovy scripts at runtime, on an Android device.
 *
 * @author Doti (inspired by Cédric Champeau)
 */
public class GrooidShell {

	private static final String DEX_IN_JAR_NAME = "classes.dex";
	private static final Attributes.Name CREATED_BY = new Attributes.Name(
			"Created-By");

 	private final File tmpDynamicFiles;
	private final ClassLoader classLoader;
	private final DexOptions dexOptions;
	private final CfOptions cfOptions;
	private Map<Integer, Map<String, Class<?>>> cachedClasses;

	public GrooidShell(File tmpDir, ClassLoader parent) {
		tmpDynamicFiles = tmpDir;
		classLoader = parent;
		dexOptions = new DexOptions();
		dexOptions.targetApiLevel = DexFormat.API_NO_EXTENDED_OPCODES;
		cfOptions = new CfOptions();
		cfOptions.positionInfo = PositionList.LINES;
		cfOptions.localInfo = true;
		//cfOptions.strictNameCheck = true;
		cfOptions.strictNameCheck = false;
		cfOptions.optimize = false;
		cfOptions.optimizeListFile = null;
		cfOptions.dontOptimizeListFile = null;
		cfOptions.statistics = false;

		cachedClasses = new LinkedHashMap<Integer, Map<String, Class<?>>>();
	}

	public Map<String, Class<?>> evaluate(String scriptText) throws Exception {
		Log.d("GrooidShell", "evaluate()");
		int scriptHashCode = scriptText.hashCode();
		
		if(cachedClasses.containsKey(scriptHashCode))
		    return cachedClasses.get(scriptHashCode);

		final Set<String> classNames = new LinkedHashSet<String>();
		final DexFile dexFile = new DexFile(dexOptions);
		CompilerConfiguration config = new CompilerConfiguration();
		config.setBytecodePostprocessor(new BytecodeProcessor() {
			@Override
			public byte[] processBytecode(String s, byte[] bytes) {
				ClassDefItem classDefItem = CfTranslator.translate(
						s + ".class", bytes, cfOptions, dexOptions);
				dexFile.add(classDefItem);
				classNames.add(s);
				return bytes;
			}
		});
		GrooidClassLoader gcl = new GrooidClassLoader(this.classLoader, config);

		long parsingStartTime = System.currentTimeMillis();
		gcl.parseClass(scriptText);
		long parsingEndTime = System.currentTimeMillis();
		Log.d("GrooidShell", String.format("parsed in %.3fs", (parsingEndTime - parsingStartTime)/1000.0));
		
		byte[] dalvikBytecode = dexFile.toDex(new OutputStreamWriter(
				new ByteArrayOutputStream()), false);
				
		Map<String, Class<?>> result = defineDynamic(classNames, dalvikBytecode);
        cachedClasses.put(scriptHashCode, result);
		return result;
	}

	private Map<String, Class<?>> defineDynamic(Set<String> classNames,
	                                            byte[] dalvikBytecode) {
		Log.d("GrooidShell", "defineDynamic()");
		File tmpDex = new File(tmpDynamicFiles, UUID.randomUUID().toString()
				+ ".jar");
		Map<String, Class<?>> result = new LinkedHashMap<String, Class<?>>();
		try {
			FileOutputStream fos = new FileOutputStream(tmpDex);
			JarOutputStream jar = new JarOutputStream(fos, makeManifest());
			JarEntry classes = new JarEntry(DEX_IN_JAR_NAME);
			classes.setSize(dalvikBytecode.length);
			jar.putNextEntry(classes);
			jar.write(dalvikBytecode);
			jar.closeEntry();
			jar.finish();
			jar.flush();
			fos.flush();
			fos.close();
			jar.close();
			DexClassLoader loader = new DexClassLoader(
					tmpDex.getAbsolutePath(),
					tmpDynamicFiles.getAbsolutePath(), null, classLoader);
			for (String className : classNames) {
				result.put(className, loader.loadClass(className));
			}
			return result;
		} catch (Throwable e) {
			Log.e("DynamicLoading", "Unable to load class", e);
		} finally {
			tmpDex.delete();
		}
		return null;
	}

	private static Manifest makeManifest() throws IOException {
		Manifest manifest = new Manifest();
		Attributes attribs = manifest.getMainAttributes();
		attribs.put(Attributes.Name.MANIFEST_VERSION, "1.0");
		attribs.put(CREATED_BY, "dx " + Version.VERSION);
		attribs.putValue("Dex-Location", DEX_IN_JAR_NAME);
		return manifest;
	}
}
