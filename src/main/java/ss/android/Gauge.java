/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * View (Ui Component) to show the gauge from scripts
 *
 * @author Doti
 */
public class Gauge extends View {

	private double prc = 0;
	private int color;
	
	public Gauge(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public Gauge(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Gauge(Context context) {
		super(context);
	}
	
	public double getPrc() {
		return prc;
	}

	public void setPrc(double prc) {
		this.prc = prc;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint p = new Paint();
		p.setColor(color);
		p.setStyle(Paint.Style.FILL);
		int width = (int) ((prc * getWidth()) / 100);
		//TODO prettier thing
		canvas.drawRect(0,0,width,getHeight(),p);
		canvas.drawCircle(width,getHeight()/2,getHeight()/2,p);
	}

	
}
