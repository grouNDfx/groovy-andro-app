/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package ss;

import java.io.File;
import java.util.List;


/**
 * The main window, device-independent
 * 
 * @author Doti
 * 
 */
public interface MainWindow extends Parameters {

	public File getDataFolder();
	
	public File getTmpFolder();
	
	/**
	 * @param fileName relative to base, or absolute name
	 */
	public String readScript(String fileName);

	public void writeScript(String fileName, boolean alsoUpdateInterface);

	public File getFileFromChooser(boolean groovyFilter, File baseFolder, String dialogTitle, Object parent);
	
	public void showAbout();

	public void changeColor(int color);

	public void setTitle(String title);

	public void updateTextAndImage();

	public void setWaiting(boolean waiting);

	public void showMessage(String text, boolean error);
	
	public String getInputString(String string, String defaultString) throws InterruptedException;

	public void setPlayingSound(boolean playingSound);
	
	public PropertiesWorker getPropertiesWorker();
	
	public ScriptContainer getScriptContainer();

	public void showFromScript(String s);
	
	public float showPopupFromScript(String s);
	
	public double showButtonFromScript(String s, double duration) throws InterruptedException;
	
	public String getStringFromScript(String text, String defaultValue) throws InterruptedException;
	
	public boolean getBooleanFromScript(String text, String yesMessage,
	                                    String noMessage) throws InterruptedException;

	public int getSelectedValueFromScript(String text, List<String> values) throws InterruptedException;
	
	public void waitFromScript(double duration) throws InterruptedException ;
	
	public void waitWithGaugeFromScript(double duration) throws InterruptedException;
	
	public void setImageFromScript(String path);
	
	public void setImageFromScript(byte[] bytes);

	public void run();

	public void stopScriptThreads();

	public String getString(String s, Object... args);

	public void dispose();
}