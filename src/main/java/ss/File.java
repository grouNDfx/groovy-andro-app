package ss;

import java.net.URI;

/*
    Copyright (C) 2011, Doti, deviatenow.com
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

    java.io.File remplacement, will be used by new File() in groovy files
    in order to exists() correct values, for example

    Android Specific !
 */
public class File extends java.io.File {

	private static final long serialVersionUID = 1L;

	public File(String dirPath, String name) {
		super(new java.io.File(dirPath).getAbsoluteFile(), name);
	}

	public File(java.io.File dir, String name) {
		super(dir.getAbsoluteFile(), name);
	}

	public File(String path) {
		super(new java.io.File(path).getAbsolutePath());
	}

	public File(URI uri) {
		super(uri);
	}
}
